import { Route, Routes } from "react-router-dom"

import MainLayout from "../layouts/MainLayout"

import MainPage from "../views/main"

const Router = () => {
  return (
    <Routes>
      <Route path="/" element={<MainLayout />}>
        <Route path="/" element={<MainPage />} />
      </Route>
    </Routes>
  )
}

export default Router
