import Table from "@mui/material/Table"
import TableBody from "@mui/material/TableBody"
import TableCell from "@mui/material/TableCell"
import TableContainer from "@mui/material/TableContainer"
import TableHead from "@mui/material/TableHead"
import TableRow from "@mui/material/TableRow"
import Paper from "@mui/material/Paper"
import {
  CardUpArrowIcon,
  BNBIcon,
  BitcoinIcon,
  EthIcon,
  LunaIcon,
  AdaIcon,
} from "../../../../assets/icons/icons"
import styles from "./styles.module.scss"
import { useWindowWidth } from "../../../../utils/useWindowSize"

function createData(name, symbol, price, changes, market, icon) {
  return { name, symbol, price, changes, market, icon }
}

const rows = [
  createData("BNB", "BNB", "$41.263,00", "+35,74%", "$784,393M", <BNBIcon />),
  createData(
    "Bitcoin",
    "BTC",
    "$41.263,00",
    "+35,74%",
    "$784,393M",
    <BitcoinIcon />
  ),
  createData(
    "Ethereum",
    "ETH",
    "$41.263,00",
    "+35,74%",
    "$784,393M",
    <EthIcon />
  ),
  createData("Terra", "TR", "$41.263,00", "+35,74%", "$784,393M", <LunaIcon />),
  createData(
    "Cardano",
    "CR",
    "$41.263,00",
    "+35,74%",
    "$784,393M",
    <AdaIcon />
  ),
]

const Trending = () => {
  const windowWidth = useWindowWidth()
  console.log("windowWidth", windowWidth)
  return (
    <div className={styles.table}>
      <div className={styles.header}>
        <h3 className={styles.headerTitle}>Trending Market</h3>
        <p className={styles.link}>View more markets</p>
      </div>

      <TableContainer
        width={"100%"}
        sx={{ boxShadow: "none" }}
        component={Paper}
      >
        <Table aria-label="simple table" stickyHeader size="medium">
          <TableHead>
            <TableRow>
              <TableCell
                width={windowWidth <= 768 ? "0%" : "10%"}
                sx={{
                  color: "#A9A9A9",
                  fontWeight: "400",
                  fontSize: "12px",
                }}
              >
                Token
              </TableCell>
              {windowWidth <= 768 ? (
                ""
              ) : (
                <TableCell
                  width={windowWidth <= 768 ? "2%" : "10%"}
                  sx={{
                    color: "#A9A9A9",
                    fontWeight: "400",
                    fontSize: "12px",
                  }}
                  align="right"
                >
                  Symbol
                </TableCell>
              )}
              <TableCell
                width={windowWidth <= 768 ? "2%" : "10%"}
                sx={{
                  color: "#A9A9A9",
                  fontWeight: "400",
                  fontSize: "12px",
                }}
                align="right"
              >
                Last Price
              </TableCell>
              <TableCell
                width={windowWidth <= 768 ? "2%" : "10%"}
                sx={{
                  color: "#A9A9A9",
                  fontWeight: "400",
                  fontSize: "12px",
                }}
                align="center"
              >
                24H Change
              </TableCell>
              <TableCell
                width={windowWidth <= 768 ? "2%" : "10%"}
                sx={{
                  color: "#A9A9A9",
                  fontWeight: "400",
                  fontSize: "12px",
                }}
                align="right"
              >
                Market Cap
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <TableRow
                key={row.name}
                sx={{
                  "&:last-child td, &:last-child th": { border: 0 },
                }}
              >
                <TableCell
                  width={windowWidth <= 768 ? "2%" : "10%"}
                  sx={{
                    border: "none",
                  }}
                  component="th"
                  scope="row"
                >
                  <span
                    style={{
                      width: "100%",
                      display: "flex",
                      gap: "12px",
                      alignItems: "center",
                      color: "#2C3131",
                      fontSize: "14px",
                      fontWeight: "600",
                    }}
                  >
                    {row.icon}
                    {row.name}
                  </span>
                </TableCell>
                {windowWidth <= 768 ? (
                  ""
                ) : (
                  <TableCell
                    width={windowWidth <= 768 ? "2%" : "10%"}
                    sx={{
                      border: "none",
                    }}
                    align="right"
                  >
                    {row.symbol}
                  </TableCell>
                )}
                <TableCell
                  width={windowWidth <= 768 ? "2%" : "10%"}
                  sx={{
                    border: "none",
                  }}
                  align="right"
                >
                  {row.price}
                </TableCell>
                <TableCell
                  width={windowWidth <= 768 ? "2%" : "10%"}
                  sx={{
                    display: "flex",
                    textAlign: "center",
                    border: "none",
                    marginLeft: windowWidth <= 768 ? "0px" : "20px",
                  }}
                >
                  <span
                    style={{
                      display: "flex",
                      gap: "4px",
                      alignItems: "center",
                      justifyContent: "center",
                      color: "#53D258",
                      fontSize: "14px",
                      fontWeight: "600",
                    }}
                  >
                    <CardUpArrowIcon />
                    {row.changes}
                  </span>
                </TableCell>
                <TableCell
                  width={windowWidth <= 768 ? "2%" : "10%"}
                  sx={{
                    border: "none",
                  }}
                  align="right"
                >
                  {row.market}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  )
}

export default Trending
