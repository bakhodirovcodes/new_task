import { CoinRightIcon } from "../../../../../../assets/icons/icons"

import styles from "./styles.module.scss"

const Coins = ({ icon, title, text }) => {
  return (
    <div className={styles.container}>
      <div className={styles.content}>
        {icon}
        <div className={styles.info}>
          <h3 className={styles.title}>{title}</h3>
          <p className={styles.text}>{text}</p>
        </div>
      </div>
      <CoinRightIcon />
    </div>
  )
}

export default Coins
