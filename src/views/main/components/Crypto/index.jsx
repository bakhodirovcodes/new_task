import { LiteIcon, ETCIcon, HodlIcon } from "../../../../assets/icons/icons"
import Coins from "./components/Coins"
import styles from "./styles.module.scss"

const data = [
  { id: 1, title: "Litecoin", text: "Added 2 days ago", icon: <LiteIcon /> },
  {
    id: 2,
    title: "Ethereum Classic",
    text: "Added 5 days ago",
    icon: <ETCIcon />,
  },
  { id: 3, title: "H0dlcoin", text: "Added 6 days ago", icon: <HodlIcon /> },
]

const Crypto = () => {
  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <h3 className={styles.title}>New Cryptocurrency</h3>
        <p className={styles.link}>See all</p>
      </div>
      <div className={styles.coins}>
        {data?.map((el) => (
          <Coins key={el.id} title={el.title} text={el.text} icon={el.icon} />
        ))}
      </div>
    </div>
  )
}
export default Crypto
