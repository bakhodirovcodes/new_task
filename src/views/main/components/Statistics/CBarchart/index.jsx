import React, { PureComponent } from "react"
import { useState } from "react"
import styles from "./styles.module.scss"
import {
  BarChart,
  Bar,
  Cell,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts"

import { useWindowWidth } from "../../../../../utils/useWindowSize"

const CustomTooltip = ({ active, payload }) => {
  if (active && payload && payload.length) {
    return (
      <div className={styles.tooltip}>
        {payload?.map((item) => (
          <div className={styles.item} key={item.value}>
            <p>${item.value}</p>
          </div>
        ))}
      </div>
    )
  }

  return null
}

export default function CBarChart({ data }) {
  const [focusBar, setFocusBar] = useState(null)
  const windowWidth = useWindowWidth()

  return (
    <ResponsiveContainer width="100%" height={246}>
      <BarChart
        width="100%"
        height="100%"
        data={data}
        margin={{
          top: 10,
          right: windowWidth <= 768 ? 22 : 0,
          left: 0,
          bottom: 0,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis
          dataKey="date"
          style={{ fontWeight: "600", fontSize: "14px", lineHeight: "0" }}
          dy={5}
          dx={-10}
          textAnchor="start"
          interval={0}
          tick={{ angle: 0 }}
        />
        <YAxis />
        <Tooltip cursor={{ fill: "transparent" }} content={<CustomTooltip />} />
        <Bar dataKey="uv" barSize={45} isAnimationActive={false}>
          {data?.map((entry, index) => (
            <Cell
              key={index}
              fill={focusBar === index ? "#2B5CE7" : "#D0DBFF"}
              onMouseEnter={() => setFocusBar(index)}
              onMouseLeave={() => setFocusBar(null)}
            />
          ))}
        </Bar>
      </BarChart>
    </ResponsiveContainer>
  )
}
