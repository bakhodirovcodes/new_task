import { useState } from "react"
import Select, { components } from "react-select"
import { DownIcon } from "../../../../assets/icons/icons"
import CBarChart from "./CBarchart"
import styles from "./styles.module.scss"

const options = [
  { value: "Balance", label: "Balance" },
  { value: "Spending", label: "Spending" },
  { value: "Saved", label: "Saved" },
]

const dateOptions = [
  { value: "2021", label: "2021" },
  { value: "2022", label: "2022" },
  { value: "2023", label: "2023" },
]

const DropdownIndicator = (props) => {
  return (
    <components.DropdownIndicator {...props}>
      <DownIcon />
    </components.DropdownIndicator>
  )
}
const IndicatorSeparator = (props) => {
  return (
    <div style={{ background: "none" }}>
      <components.IndicatorSeparator {...props} />
    </div>
  )
}

const topData = [
  {
    date: "Jan",
    uv: 4300,
  },
  {
    date: "Feb",
    uv: 7000,
  },
  {
    date: "Mar",
    uv: 7800,
  },
  {
    date: "Apr",
    uv: 8000,
  },
  {
    date: "May",
    uv: 5500,
  },
  {
    date: "Jun",
    uv: 8300,
  },
  {
    date: "Jul",
    uv: 7800,
  },
  {
    date: "Aug",
    uv: 3000,
  },
  {
    date: "Sept",
    uv: 7000,
  },
  {
    date: "Okt",
    uv: 8000,
  },
  {
    date: "Nov",
    uv: 2500,
  },
  {
    date: "Des",
    uv: 6200,
  },
]

const Statistics = () => {
  const [selectedOption, setSelectedOption] = useState("")
  const [datesOption, setDatesOption] = useState("")
  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <h3 className={styles.title}>Statistics</h3>
        <div className={styles.selects}>
          <Select
            defaultValue={selectedOption}
            onChange={setSelectedOption}
            options={options}
            placeholder=""
            styles={{
              control: (baseStyles, state) => ({
                ...baseStyles,
                width: "105px",
                minHeight: "28px",
                color: "#626262",
                fontSize: "12px",
                fontWeight: "400",
                borderColor: state.isFocused ? "#2752E7" : "#CACACA",
              }),
              menu: (baseStyles, state) => ({
                ...baseStyles,
                width: "100px",
              }),
              option: (baseStyles, state) => ({
                ...baseStyles,
                color: "#626262",
                fontSize: "12px",
                fontWeight: "400",
              }),
              downChevron: { display: "none" },
            }}
            components={{ IndicatorSeparator, DropdownIndicator }}
          />
          <Select
            defaultValue={datesOption}
            onChange={setDatesOption}
            options={dateOptions}
            placeholder=""
            styles={{
              control: (baseStyles, state) => ({
                ...baseStyles,
                width: "80px",
                minHeight: "28px",
                color: "#626262",
                fontSize: "12px",
                fontWeight: "400",
                borderColor: state.isFocused ? "#2752E7" : "#CACACA",
              }),
              menu: (baseStyles, state) => ({
                ...baseStyles,
                width: "100px",
              }),
              downChevron: { display: "none" },
              option: (baseStyles, state) => ({
                ...baseStyles,
                color: "#626262",
                fontSize: "12px",
                fontWeight: "400",
              }),
            }}
            components={{ IndicatorSeparator, DropdownIndicator }}
          />
        </div>
      </div>

      <div className={styles.graph}>
        <CBarChart data={topData} />
      </div>
    </div>
  )
}

export default Statistics
