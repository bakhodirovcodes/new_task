import {
  CardUpArrowIcon,
  BTBNewIcon,
  BTCNewIcon,
  ETHNewIcon,
  LUNANewIcon,
  RedArrpwDownIcon,
} from "../../../../assets/icons/icons"

import styles from "./styles.module.scss"

const data = [
  {
    id: 1,
    title: "BNB",
    text: "BNB",
    cost: "$41.263,00",
    subIcon: <CardUpArrowIcon />,
    icon: <BTBNewIcon />,
    count: "+35,74%",
  },
  {
    id: 2,
    title: "BNB",
    text: "BNB",
    cost: "$41.263,00",
    subIcon: <CardUpArrowIcon />,
    icon: <BTCNewIcon />,
    count: "+35,74%",
  },
  {
    id: 3,
    title: "BNB",
    text: "BNB",
    cost: "$41.263,00",
    subIcon: <CardUpArrowIcon />,
    icon: <ETHNewIcon />,
    count: "+35,74%",
  },
  {
    id: 4,
    title: "Terra",
    text: "LUNA",
    cost: "$41.263,00",
    subIcon: <RedArrpwDownIcon />,
    icon: <LUNANewIcon />,
    count: "+35,74%",
    red: true,
  },
]

const TrendingRes = () => {
  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <h3 className={styles.title}>Trending Market</h3>
        <p className={styles.link}>View all</p>
      </div>

      <div className={styles.cards}>
        {data?.map((el) => (
          <div key={el.id} className={styles.card}>
            <div className={styles.content}>
              {el.icon}
              <div className={styles.info}>
                <p className={styles.infoTitle}>{el.title}</p>
                <p className={styles.infoText}>{el.text}</p>
              </div>
            </div>
            <div className={styles.costs}>
              <p className={styles.cost}>{el.cost}</p>
              <div className={styles.percent}>
                {el.subIcon}
                <p className={el.red ? styles.countRed : styles.count}>
                  {el.count}
                </p>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  )
}

export default TrendingRes
