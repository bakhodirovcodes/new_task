import { useForm } from "react-hook-form"

import { BitcoinIcon, EthIcon, SwappIcon } from "../../../../assets/icons/icons"
import TypeInput from "../../../../components/FormElements/Inputs/FormInput"
import styles from "./styles.module.scss"

const Swapper = () => {
  const { control } = useForm({})

  return (
    <div className={styles.container}>
      <div className={styles.names}>
        <span>Buy</span>
        <div className={styles.line}></div>
        <span>Sell</span>
      </div>

      <div className={styles.resName}>
        <div className={styles.buy}>
          <p>Buy</p>
        </div>
        <div className={styles.sell}>
          <p>Sell</p>
        </div>
      </div>

      <div className={styles.pirce}>
        <p className={styles.title}>Ethereum Price</p>
        <p className={styles.count}>$3.110,31</p>
      </div>

      <div className={styles.forms}>
        <TypeInput
          control={control}
          marketIcon={<BitcoinIcon />}
          name="first_input"
          marketName="BTC"
        />
        <TypeInput
          control={control}
          marketIcon={<EthIcon />}
          name="second_input"
          marketName="ETH"
        />
        <div className={styles.swapp}>
          <SwappIcon />
        </div>

        <button>Buy ETH</button>
      </div>
    </div>
  )
}

export default Swapper
