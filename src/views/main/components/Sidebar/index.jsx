import styles from "./styles.module.scss"
import {
  HomeIcon,
  AnalyticsIcon,
  DeskIcon,
  WalletIcon,
  ChartIcon,
  SettingsIcon,
  InfoIcon,
  LogOutIcon,
} from "../../../..//assets/icons/icons"

const mainbarData = [
  { id: 1, icon: <HomeIcon />, name: "Dashboard", active: true },
  { id: 2, icon: <AnalyticsIcon />, name: "Analytics" },
  { id: 3, icon: <DeskIcon />, name: "My Portfolio" },
  { id: 4, icon: <WalletIcon />, name: "My Wallets" },
  { id: 5, icon: <ChartIcon />, name: "Exchanges" },
]

const settingsbarData = [
  { id: 1, icon: <SettingsIcon />, name: "Settings" },
  { id: 2, icon: <InfoIcon />, name: "Help" },
  { id: 3, icon: <LogOutIcon />, name: "Log out" },
]

const SideBar = () => {
  return (
    <div className={styles.container}>
      <div className={styles.mainbar}>
        {mainbarData?.map((item) => (
          <div
            key={item.id}
            className={`${styles.barContents} ${
              item?.active ? styles.active : ""
            }`}
          >
            <span className={styles.icon}>{item?.icon}</span>
            <h3 className={styles.title}>{item?.name}</h3>
          </div>
        ))}

        <div className={styles.line}></div>
      </div>

      <div className={styles.settingsBar}>
        {settingsbarData?.map((el) => (
          <div key={el?.id} className={styles.secondBarContents}>
            <span className={styles.icon}>{el?.icon}</span>
            <h3 className={styles.title}>{el?.name}</h3>
          </div>
        ))}
      </div>
    </div>
  )
}

export default SideBar
