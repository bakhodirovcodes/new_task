import { useForm } from "react-hook-form"

import Elipse from "../../../../assets/images/Ellipse1.png"
import Elipse1 from "../../../../assets/images/Ellipse2.png"
import Elipse2 from "../../../../assets/images/Ellipse3.png"
import Elipse3 from "../../../../assets/images/Ellipse4.png"
import { AddIcon, ArrowRightIcon } from "../../../../assets/icons/icons"
import TypeInput from "../../../../components/FormElements/Inputs/FormInput"
import { useWindowWidth } from "../../../../utils/useWindowSize"

import styles from "./styles.module.scss"

const Transfer = () => {
  const { control } = useForm({})
  const windowWidth = useWindowWidth()
  return (
    <div className={styles.container}>
      <h3 className={styles.title}>Quick Transfer</h3>
      <div className={styles.add}>
        <img src={Elipse} alt="elipse" />
        <img src={Elipse1} alt="elipse" />
        {windowWidth <= 768 ? "" : <img src={Elipse2} alt="elipse" />}
        {windowWidth <= 375 ? <img src={Elipse2} alt="elipse" /> : ""}
        <img src={Elipse3} alt="elipse" />
        <AddIcon />
      </div>
      <div className={styles.form}>
        <label>Amount</label>
        <TypeInput
          withLetft={true}
          control={control}
          noArrow={true}
          name="amount"
          leftIcon="$"
        />

        <button>Transfer {<ArrowRightIcon />}</button>
      </div>
    </div>
  )
}

export default Transfer
