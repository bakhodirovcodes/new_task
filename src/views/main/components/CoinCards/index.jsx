import styles from "./styles.module.scss"

const CoinCards = ({
  icon,
  title,
  img,
  count,
  subIcon,
  percent,
  active,
  persentRed,
}) => {
  return (
    <div className={active ? styles.activeContainer : styles.container}>
      <div className={styles.header}>
        <span className={styles.icon}>{icon}</span>
        <p className={styles.title}>{title}</p>
        <span className={styles.graph}>{img}</span>
      </div>
      <div className={styles.counts}>
        <p className={styles.number}>{count}</p>
        <div className={styles.subInfo}>
          <span>{subIcon}</span>
          <p className={persentRed ? styles.redPercent : styles.percent}>
            {percent}
          </p>
        </div>
      </div>
    </div>
  )
}

export default CoinCards
