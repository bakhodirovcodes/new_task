import SideBar from "./components/Sidebar"
import CoinCards from "./components/CoinCards"
import Trending from "./components/Trending"
import Statistics from "./components/Statistics"
import Swapper from "./components/Swapper"
import Transfer from "./components/Transfer"
import Crypto from "./components/Crypto"
import TrendingRes from "./components/TendingRes"
import {
  GreenVectorIcon,
  RedVectorIcon,
  CardWalletIcon,
  CardUpArrowIcon,
  RefreshIcon,
  MoneyIcon,
  RedArrpwDownIcon,
} from "../../assets/icons/icons"

import styles from "./styles.module.scss"

const coinCardsData = [
  {
    id: 1,
    title: "Balance",
    img: <GreenVectorIcon />,
    icon: <CardWalletIcon />,
    count: "$40,000.063",
    percent: "+35,74%",
    subIcon: <CardUpArrowIcon />,
    active: true,
  },
  {
    id: 2,
    title: "Spending",
    img: <RedVectorIcon />,
    icon: <RefreshIcon />,
    count: "-$103,000",
    percent: "+10,74%",
    subIcon: <RedArrpwDownIcon />,
    persentRed: true,
  },
  {
    id: 3,
    title: "Saved",
    img: <GreenVectorIcon />,
    icon: <MoneyIcon />,
    count: "$103,000",
    percent: "+10,74%",
    subIcon: <CardUpArrowIcon />,
  },
]

const MainPage = () => {
  return (
    <div className={styles.container}>
      <SideBar />
      <div className={styles.mainBody}>
        <div className={styles.bodyContent}>
          <div className={styles.cards}>
            {coinCardsData?.map((el) => (
              <CoinCards
                key={el?.id}
                title={el?.title}
                icon={el?.icon}
                img={el?.img}
                count={el?.count}
                percent={el?.percent}
                subIcon={el?.subIcon}
                active={el?.active}
                persentRed={el?.persentRed}
              />
            ))}
          </div>
          <div className={styles.statistics}>
            <Statistics />
          </div>

          <div className={styles.trendinRes}>
            <TrendingRes />
          </div>

          <div className={styles.resSwapper}>
            <Swapper />
          </div>

          <div className={styles.trending}>
            <Trending />
          </div>
        </div>
        <div className={styles.sells}>
          <div className={styles.statusSwapper}>
            <Swapper />
          </div>
          <div className={styles.transfers}>
            <Transfer />
            <Crypto />
          </div>
          <div className={styles.restransfers}>
            <Crypto />
            <Transfer />
          </div>
        </div>
      </div>
    </div>
  )
}

export default MainPage
