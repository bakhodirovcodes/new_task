import LOGO from "../../assets/icons/logo.svg"
import BELL from "../../assets/icons/bell.svg"
import PERSON from "../../assets/images/navImg.png"
import Arrow_Down from "../../assets/icons/arro_down.svg"
import {
  SearchIcon,
  NewSearchIcon,
  MenuBtnIcon,
} from "../../assets/icons/icons"

import styles from "./styles.module.scss"

const Header = () => {
  return (
    <div className={styles.header}>
      <div className={styles.container}>
        <div className={styles.logo}>
          <img src={LOGO} alt="logo" />
        </div>
        <div className={styles.nav}>
          <div className={styles.resMenuBtn}>
            <MenuBtnIcon />
          </div>
          <div className={styles.navTitle}>
            <h2 className={styles.title}>Welcome Zarror!</h2>
            <p className={styles.text}>
              Hope you are healthy and happy today...
            </p>
          </div>

          <div className={styles.filter}>
            <div className={styles.searchInputWrapper}>
              <span className={styles.leftIcons}>{<SearchIcon />}</span>
              <input type="text" placeholder="Search..." />
            </div>
            <div className={styles.resIcon}>
              <NewSearchIcon />
            </div>
            <img className={styles.bell} src={BELL} alt="bell" />
            <div className={styles.personInfo}>
              <img src={PERSON} alt="person" />
              <img className={styles.arrowImg} src={Arrow_Down} alt="arrow" />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Header
