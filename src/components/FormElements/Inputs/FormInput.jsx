import { Controller } from "react-hook-form"
import { NumericFormat } from "react-number-format"

import { ExpendDownIcon } from "../../../assets/icons/icons"
import styles from "./styles.module.scss"

const TypeInput = ({
  disabled,
  name = "",
  defaultValue,
  control,
  icon,
  endIcon,
  leftIcon,
  noArrow,
  setValue,
  errors,
  validation = false,
  validationSingle,
  autoComplete = "off",
  required = false,
  leftImg,
  placeholder,
  inputDateProps = {},
  buyForMeBg,
  withLetft,
  endAdornment = "",
  marketIcon,
  marketName,
  ...restProps
}) => {
  return (
    <div>
      <Controller
        control={control}
        rules={
          validation
            ? {
                required: {
                  value: validation,
                  message: "obligatoryField",
                },
              }
            : validationSingle
        }
        defaultValue={defaultValue}
        name={name}
        render={({ field: { value, onChange, name } }) => {
          return (
            <div
              style={
                errors?.[`${name}`]?.message ? { borderColor: "#F76659" } : {}
              }
              className={`${
                withLetft ? styles.withLeftInputWrapper : styles.inputWrapper
              } ${disabled && styles.disabled} `}
            >
              {leftImg && (
                <span className={styles.leftIcon}>
                  <img src={leftImg} alt="leftImg" />
                </span>
              )}
              {leftIcon && <span className={styles.leftIcons}>{leftIcon}</span>}
              <NumericFormat
                value={value}
                thousandsGroupStyle="thousand"
                thousandSeparator=" "
                decimalSeparator="."
                displayType="input"
                autoComplete="off"
                placeholder={placeholder}
                allowNegative={false}
                name={name}
                onChange={(e) => {
                  onChange(e.target.value)
                  // customOnChange(e)
                }}
                className={styles[buyForMeBg]}
                {...restProps}
              />

              <span className={styles.rightIcon} onClick={setValue}>
                {marketIcon}{" "}
                <span className={styles.marketName}>{marketName}</span>{" "}
                {noArrow ? "" : <ExpendDownIcon />}
              </span>
            </div>
          )
        }}
      />
      {errors?.[`${name}`]?.message && (
        <span className={styles.errMsg}>{errors?.[`${name}`]?.message}</span>
      )}
    </div>
  )
}

export default TypeInput
