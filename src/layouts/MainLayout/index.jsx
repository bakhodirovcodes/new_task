import { Outlet } from "react-router-dom"
import Header from "../../components/header"

import styles from "./styles.module.scss"

const MainLayout = () => {
  return (
    <div className={styles.layout}>
      <Header />
      <div className={styles.layout__content}>
        <Outlet />
      </div>
    </div>
  )
}

export default MainLayout
